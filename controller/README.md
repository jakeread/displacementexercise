# DEX Controller

I've decided to roll a standalone controller for DEX. I've made a little ad-hoc breadboard circuit to connect one stepper driver, one load cell amp, to an Adafruit Feather M4 - an arduino variant. 

The controller is fairly simple. The embedded code (which can be built with Arduino) reads command codes from the usb-serial port. These commands tell the machine to:
- move a number of steps 
- tare the loadcell 
- read the loadcell 

Instructions to the machine are done in low-level appropriate data types: steps are steps, not mm. Readings are returned as raw ADC readings. Conversions between machine-units and real-world units are done in JavaScript.

One javascript launches a webserver: that's `js/dexs.js` - you should be able to run it (after installing node) by navigating to the `js` directory here and doing `node dexs.js`. It will announce an IP address and port, point your browser at that. Important! Launch the tool when the DEX machine is already plugged into USB.

The client javascript can jog the machine, tare the loadcell, and perform tests. Tests are saved as .csv files.

Hopefully enough of the code is self-explanatory, more documentation can come if it's requested. 

![controller](/images/2020-07-11_dex-tools.png)
![controller](/images/2020-07-12_dex-on-desk-02.jpg)

## Install

``Install Node.JS``  
``npm install express``  
``npm install ws``  
``npm install serialport``  
``npm install esm``  