#include <Arduino.h>

// system
#include "drivers/indicators.h"
#include "comm/cobs.h"
#include "comm/cobsserial.h"

// loadcell / stepper 
#include "lib/HX711.h"
#include "lib/AccelStepper.h"

#define LOADCELL_DOUT_PIN 12
#define LOADCELL_SCK_PIN 11
#define LOADCELL_OFFSET 50682624
#define LOADCELL_DIVIDER 5895655

#define STEPPER_STEP_PIN 9
#define STEPPER_DIR_PIN 10
#define STEPPER_ENABLE_PIN 5

HX711 loadcell;
AccelStepper stepper(AccelStepper::DRIVER, 9, 10);

// using A4988 step driver, 
// want to limit to 1A current for thermal safety, 
// vref = 8 * I_max * 0.050 // is 8 * 1A * 50mOhm 
// so, target vref at 0.4v

void enableStepDriver(void){
  digitalWrite(STEPPER_ENABLE_PIN, LOW);
}

void disableStepDriver(void){
  digitalWrite(STEPPER_ENABLE_PIN, HIGH);
}

COBSSerial* cobsSerial = new COBSSerial();

void setup() {
  // do osap setup, 
  ERRLIGHT_SETUP;
  CLKLIGHT_SETUP;
  cobsSerial->init();
  // do application setup, 
  // EN pin on step driver 
  pinMode(5, OUTPUT);
  disableStepDriver();
  // stepper basics hello 
  stepper.setMaxSpeed(2500);
  stepper.setAcceleration(5000);
  // loacell 
  loadcell.begin(LOADCELL_DOUT_PIN, LOADCELL_SCK_PIN);
  loadcell.set_scale(LOADCELL_DIVIDER);
  loadcell.set_offset(LOADCELL_OFFSET);
  loadcell.tare();
}

uint8_t* pck;
uint16_t pl = 0;

uint8_t res[1024];
uint16_t rl = 0;

#define DEXKEY_LOADCELLREADING 12 
#define DEXKEY_LOADCELLTARE 14
#define DEXKEY_STEPS 15
#define DEXKEY_MOTORENABLE 16

uint16_t one = 1;
int32_t stepsToTake = 0;
int32_t position = 0;
uint8_t retries = 0;

void loop() {
  stepper.run();
  cobsSerial->loop();
  if(cobsSerial->hasPacket()){
    CLKLIGHT_TOGGLE;
    cobsSerial->getPacket(&pck, &pl);
    switch(pck[0]){
      case DEXKEY_LOADCELLREADING:
        while(!loadcell.is_ready()){
          delay(100);
          retries ++;
          if(retries > 10) break;
        }
        if(loadcell.is_ready()){
          retries = 0;
          int32_t reading = loadcell.get_value(5);
          rl = 0;
          res[rl ++] = DEXKEY_LOADCELLREADING;
          ts_writeInt32(reading, res, &rl);
          //debugmsg("reads: " + String(reading));
        } else {
          debugmsg("loadcell not ready");
        }
        break;
      case DEXKEY_LOADCELLTARE:
        loadcell.tare();
        rl = 0;
        res[rl ++] = DEXKEY_LOADCELLTARE;
        break;
      case DEXKEY_STEPS:
        stepsToTake = 0;
        one = 1;
        ts_readInt32(&stepsToTake, pck, &one);
        //debugmsg("to take steps: " + String(stepsToTake));
        stepper.runToNewPosition(position + stepsToTake);
        position += stepsToTake;
        rl = 0;
        res[rl ++] = DEXKEY_STEPS;
        ts_writeInt32(stepsToTake, res, &rl);
        break;
      case DEXKEY_MOTORENABLE:
        rl = 0;
        res[rl ++] = DEXKEY_MOTORENABLE;
        if(pck[1] > 0){
          enableStepDriver();
          res[rl ++] = 1;
        } else {
          disableStepDriver();
          res[rl ++] = 0;
        }
      default:
        // no-op 
        break;
    }
    if(rl > 0){
      cobsSerial->sendPacket(res, rl);
    }
    cobsSerial->clearPacket();
  }
} // end loop 
