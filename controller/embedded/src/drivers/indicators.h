// for feather M4 express

#define CLKLIGHT_BM (uint32_t)(1 << 0)
#define CLKLIGHT_ON digitalWrite(13, HIGH)
#define CLKLIGHT_OFF digitalWrite(13, LOW)
#define CLKLIGHT_TOGGLE digitalWrite(13, !digitalRead(13))
#define CLKLIGHT_SETUP pinMode(13, OUTPUT)

//
// there's a neopixel on the board that could be used for this,
// maybe later 

#define ERRLIGHT_BM (uint32_t)(1 << 0)
#define ERRLIGHT_ON 
#define ERRLIGHT_OFF 
#define ERRLIGHT_TOGGLE 
#define ERRLIGHT_SETUP 
