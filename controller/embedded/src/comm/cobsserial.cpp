/*
cobsserial.cpp 

COBS delineated serial packets 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include "cobsserial.h"

COBSSerial::COBSSerial(){
}

void COBSSerial::init(void){
  // start the port... 
  Serial.begin(9600);
}

void COBSSerial::loop(void){
  while(Serial.available()){
    if(_pl > 0) break;
    _encodedPacket[_bwp] = Serial.read();
    if(_encodedPacket[_bwp] == 0){
      // indicate we recv'd zero
      // CLKLIGHT_TOGGLE;
      // decode from rx-ing frame to interface frame,
      size_t dcl = cobsDecode(_encodedPacket, _bwp, _packet);
      _pl = dcl; // this frame now available, has this length,
      // reset byte write pointer
      _bwp = 0;
    } else {
      _bwp ++;
    }
  }
}

boolean COBSSerial::hasPacket(void){
  if(_pl > 0){
    return true;
  } else {
    return false;
  }
}

void COBSSerial::getPacket(uint8_t **pck, uint16_t *pl){
  *pck = _packet;
  *pl = _pl;
}

void COBSSerial::clearPacket(void){
  // frame consumed, clear to write-in,
  _pl = 0;
}

void COBSSerial::sendPacket(uint8_t *pck, uint16_t pl){
  size_t encLen = cobsEncode(pck, pl, _encodedOut);
  Serial.write(_encodedOut, encLen);
}
