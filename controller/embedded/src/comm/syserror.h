#ifndef SYSERROR_H_
#define SYSERROR_H_

#include <arduino.h>
#include "./drivers/indicators.h"
#include "cobs.h"
#include "ts.h"

#define DEXKEY_DEBUGMSG 11

void debugmsg(String msg);

#endif
