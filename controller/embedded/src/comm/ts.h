/*
osap/ts.h

typeset / keys / writing / reading

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#include <arduino.h>

// -------------------------------------------------------- Reading and Writing

void ts_writeBoolean(boolean val, unsigned char *buf, uint16_t *ptr);

void ts_readUint16(uint16_t *val, uint8_t *buf, uint16_t *ptr);
void ts_writeUint16(uint16_t val, unsigned char *buf, uint16_t *ptr);

void ts_writeUint32(uint32_t val, unsigned char *buf, uint16_t *ptr);

void ts_readInt32(int32_t *val, unsigned char *buf, uint16_t *ptr);
void ts_writeInt32(int32_t val, unsigned char *buf, uint16_t *ptr);

void ts_writeString(String val, unsigned char *buf, uint16_t *ptr);
