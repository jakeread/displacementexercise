/*
cobsserial.h

COBS delineated serial packets 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2019

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the squidworks and ponyo projects.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

#ifndef COBSSERIAL_H_
#define COBSSERIAL_H_

#include <arduino.h>
#include "cobs.h"
#include "syserror.h"
#include "./drivers/indicators.h"

#define VPUSB_SPACE_SIZE 1028

class COBSSerial {
private:
  // unfortunately, looks like we need to write-in to temp,
  // and decode out of that
  uint8_t _encodedPacket[VPUSB_SPACE_SIZE];
  uint8_t _packet[VPUSB_SPACE_SIZE];
  uint16_t _pl = 0;
  uint16_t _bwp = 0; // byte write pointer,
  uint8_t _lastPacket = 0; // last packet written into
  // outgoing cobs-copy-in,
  uint8_t _encodedOut[VPUSB_SPACE_SIZE];
  // this is just for debug,
  uint8_t _ringPacket[VPUSB_SPACE_SIZE];
public:
  COBSSerial();
  // props
  uint16_t maxSegLength = VPUSB_SPACE_SIZE - 6;
  // code
  void init(void);
  void loop(void);
  // handle incoming frames
  boolean hasPacket(void); // check existence 
  void getPacket(uint8_t** pck, uint16_t* pl);
  void clearPacket(void);
  // dish outgoing frames, and check if cts
  void sendPacket(uint8_t *pck, uint16_t pl);
};

#endif
