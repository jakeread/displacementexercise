/*
dex-ui.js

dex interface client side 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the open systems assembly protocol (OSAP) project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

import LineChart from "./utes/lineChart.js"

export default function DEXUI(){
    // the machine object, 
    let dex = null 
    this.start = (machine) => {
        console.warn('alive')
        dex = machine 
        clear()
    }
    // when lower levels bail, refreshing is the move 
    this.connectionError = (msg) => {
        console.error('HALT')
    }

    // elements 
    let dom = $('#wrapper').get(0)

    // UI util 
    let setPosition = (div, x, y) => {
        $(dom).append(div)
        $(div).css('left', `${x}px`).css('top', `${y}px`)
    }

    // hold flag 
    let waitUi = $('<div>').addClass('flag').get(0)
    setPosition(waitUi, 180, 50)
    let waitStatus = false 
    let waiting = () => {
        waitStatus = true 
        $(waitUi).css('background-color', '#f7bbb7')
    }
    let clear = () => {
        waitStatus = false 
        $(waitUi).css('background-color', '#b7f7c3')
    }
    let isWaiting = () =>{
        return waitStatus
    }
    clear()

    // incremental stacks 
    let ypos = 50 
    let yinc = 30

    // position / zero 
    let position = 0 
    let posDisplay = $('<div>').addClass('button').get(0)
    setPosition(posDisplay, 50, ypos)
    posDisplay.update = (num) => {
        position = num 
        $(posDisplay).text(`${num.toFixed(5)}mm`)
    }
    posDisplay.update(position)
    $(posDisplay).click((evt) => {
        position = 0 
        posDisplay.update(position)
    })

    let enableButton = $('<div>').addClass('button').get(0)
    setPosition(enableButton, 50, ypos += yinc)
    let motorEnabled = false 
    enableButton.update = (state) => {
        if(state){
            motorEnabled = true
            $(enableButton).text('disable motor')
            $(enableButton).css('background-color', '#b7f7c3')
        } else {
            motorEnabled = false 
            $(enableButton).text('enable motor')
            $(enableButton).css('background-color', '#f7bbb7')
        }
    }
    enableButton.update(motorEnabled)
    $(enableButton).click((evt) => {
        if(isWaiting()) return 
        waiting()
        let req = !motorEnabled
        dex.setMotorEnable(req).then((state) => {
            clear()
            enableButton.update(state)
        }).catch((err) => {
            clear()
            console.error(err)  
        })
    })

    // increment-by 
    let incBy = $('<input type="text" value="5">').addClass('inputwrap').get(0)
    setPosition(incBy, 50, ypos += yinc)
    //console.log(incBy.value)

    // up-button 
    let incUp = $('<div>').addClass('button').get(0)
    setPosition(incUp, 50, ypos += yinc)
    $(incUp).text('jog up')
    $(incUp).click((evt) => {
        if(isWaiting()) return 
        waiting()
        dex.step(parseFloat(incBy.value)).then((increment) => {
            clear()
            posDisplay.update(position += increment)
        }).catch((err) => {
            clear()
            console.error(err)
        })
    })
    // down-button 
    let incDown = $('<div>').addClass('button').get(0)
    setPosition(incDown, 50, ypos += yinc)
    $(incDown).text('jog down')
    $(incDown).click((evt) => {
        if(isWaiting()) return 
        waiting()
        dex.step(parseFloat(incBy.value) * -1).then((increment) => {
            clear()
            posDisplay.update(position += increment)
        }).catch((err) => {
            clear()
            console.error(err)
        })
    })

    // reading / zero 
    let load = 0 
    let loadDisplay = $('<div>').addClass('button').get(0)
    setPosition(loadDisplay, 50, ypos += yinc + 10)
    loadDisplay.update = (num) => {
        load = num
        $(loadDisplay).text(`${num.toFixed(3)}N`)
    }
    loadDisplay.update(load)
    $(loadDisplay).click((evt) => {
        if(isWaiting()) return 
        waiting()
        dex.tareLoadcell().then(() => {
            clear()
            loadDisplay.update(0)
        }).catch((err) => {
            clear()
            console.error(err)
        })
    })
    // read request 
    let readReq = $('<div>').addClass('button').get(0)
    setPosition(readReq, 50, ypos += yinc)
    $(readReq).text('read loadcell')
    $(readReq).click((evt) => {
        if(isWaiting()) return 
        waiting()
        dex.readLoadcell().then((load) => {
            clear()
            loadDisplay.update(load)
        }).catch((err) => {
            clear()
            console.error(err)
        })
    })

    // testing 
    // test-by
    let testStep = $('<input type="text" value="0.005">').addClass('inputwrap').get(0)
    setPosition(testStep, 50, ypos += yinc + 10)
    // start / stop testing 
    let testButton = $('<div>').addClass('button').get(0)
    setPosition(testButton, 50, ypos += yinc)
    let testing = false 
    testButton.update = (state) => {
        if(state){
            testing = true
            $(testButton).text('stop testing')
            $(testButton).css('background-color', '#f7bbb7')
        } else {
            testing = false 
            $(testButton).text('start testing')
            $(testButton).css('background-color', '#b7f7c3')
        }
    }
    testButton.update(testing)
    $(testButton).click((evt) => {
        if(testing){
            // stop 
            testButton.update(false)
        } else {
            // stop testing 
            runTest()
        }
    })

    // util 
    let stringDate = () => {
        let now = new Date()
        return `${now.getFullYear()}-${now.getMonth()}-${now.getDate()}_${now.getHours()}-${now.getMinutes()}`
    }
    // testing 
    let testData = []
    let runTest = async () => {
        if(isWaiting()) return 
        position = 0
        let startTime = performance.now()
        testData = []
        let drawData = []
        testButton.update(true)
        waiting()
        while(testing){
            try {
                let time = (performance.now() - startTime) / 1000
                // read 1st, then move, 
                let reading = await dex.readLoadcell()
                loadDisplay.update(reading)
                // store it, time, extension, load 
                testData.push([time, position, reading])
                drawData.push([position, reading])
                // and now, 
                let increment = await dex.step(testStep.value)
                posDisplay.update(position += increment)
                chart.draw(drawData)
            } catch (err) {
                console.error(err)
                testButton.update(false)
            }
        }
        clear()
    }

    // and finally, the doodling 
    let chart = new LineChart()
    setPosition(chart.de, 220, 50)

    // read request 
    let saveReq = $('<div>').addClass('button').get(0)
    setPosition(saveReq, 50, ypos += yinc)
    $(saveReq).text('save data')
    $(saveReq).click((evt) => {
        console.log()
        dish(testData, 'csv', `${stringDate()}_dextest`)
    })

    let dish = (obj, format, name) => {
        // serialize the thing
        let url = null
        if(format == 'json'){
            url = URL.createObjectURL(new Blob([JSON.stringify(obj, null, 2)], {
                type: "application/json"
            }))
        } else if (format == 'csv') {
            let csvContent = "data:text/csv;charset=utf-8," //+ obj.map(e => e.join(',')).join('\n')
            csvContent += "Time,Extension,Load\n"
            csvContent += "(sec),(mm),(N)\n"
            for(let line of obj){
                csvContent += `${line[0].toFixed(3)},${line[1].toFixed(4)},${line[2].toFixed(4)}\n`
            }
            console.log(csvContent)
            url = encodeURI(csvContent)
        }
        // hack to trigger the download,
        let anchor = $('<a>ok</a>').attr('href', url).attr('download', name + `.${format}`).get(0)
        $(document.body).append(anchor)
        anchor.click()
    }
}

