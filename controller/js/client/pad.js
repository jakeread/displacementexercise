/*
domain.js

click-to-go, other virtual machine dwg 

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the open systems assembly protocol (OSAP) project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

'use strict' 

import dt from './drawing/domtools.js'

export default function Pad() {
    // machine size : pixel size
    let psize = [500, 500]
    let msize = [200, 200]
    let scale = [msize[0] / psize[0], msize[1] / psize[1]]
    // setup the pad
    let dom = $('.plane').get(0)
    let pad = $('<div>').addClass('pad').get(0)
    $(pad).css('background-color', '#c9e5f2').css('width', `${psize[0]}px`).css('height', `${psize[1]}px`)
    $(dom).append(pad)
    let dft = { s: 1, x: -550, y: -550, ox: 0, oy: 0 }
    dt.writeTransform(pad, dft)
    // drawing lines, 
    let segs = [] // ramps: have .pi and .pf 
    let drawSegs = () => {
        $(pad).children('.svgcont').remove() // rm all segs 
        for(let seg of segs){
            let del = [seg.pf[0] - seg.pi[0], seg.pf[1] - seg.pi[1]]
            $(pad).append(dt.svgLine(seg.pi[0] / scale[0], seg.pi[1] / scale[1], del[0]/scale[1], del[1]/scale[1]))
        }
    }

    this.onNewTarget = (pos) => {
        console.warn('bind this')
    }

    this.addSeg = (ramp) => {
        //console.warn('draw', ramp.pi, ramp.pf)
        segs.push(ramp)
        if(segs.length > 10) segs.shift()
        drawSegs()
    }
    
    // handle clicks 
    pad.addEventListener('click', (evt) => {
        if(evt.target != pad) return 
        // scaled to machine spec, and invert y pixels -? logical 
        let pos = [evt.layerX * scale[0], evt.layerY * scale[1]]
        //console.warn(`X: ${pos[0].toFixed(2)}, Y: ${pos[1].toFixed(2)}`)
        this.onNewTarget(pos)
    })
}