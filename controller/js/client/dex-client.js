/*
dex-client.js

dex tool client side

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the open systems assembly protocol (OSAP) project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

'use strict'

import { TS } from '../core/ts.js'
import LeastSquares from './utes/lsq.js'
import DEXUI from './dex-ui.js'

import * as dat from './libs/dat.gui.module.js'

console.log("hello-dex-tools")

// -------------------------------------------------------- KEYS / TOPLEVEL UI

document.addEventListener('keydown', (evt) => {
  console.log(`keycode ${evt.keyCode}`)
  switch (evt.keyCode) {
    case 80: // 'p'
      if(wscPort.send){
        console.log('pinging...')
        wscPort.send(Uint8Array.from([0,1,2]))
      } else {
        console.log('port closed')
      }
      break;
    case 82: // 'r'
      // read loadcell, 
      console.log('reading cell...')
      dex.readLoadcell().then((count) => {
        console.log('resolves count', count)
      }).catch((err) => {
        console.error(err)
      })
      break;
    case 69: // 'e'
      dex.setMotorEnable(true).then((val) => {
        console.log('motor is ', val)
      }).catch((err) => {
        console.error(err)
      })
      break;
    case 68: // 'd'
      dex.setMotorEnable(false).then((val) => {
        console.log('motor is ', val)
      }).catch((err) => {
        console.error(err)
      })
      break;
    case 83: // 's'
      dex.step(-10000).then((increment) => {
        console.log('motor stepped', increment)
      }).catch((err) => {
        console.error(err)
      })
      break;
  }
})

// -------------------------------------------------------- Button Smashing 

let UI = new DEXUI()

// -------------------------------------------------------- DEX Virtual Machine

let TIMEOUT_MS = 5000
let DEXKEY_DEBUGMSG = 11
let DEXKEY_LOADCELLREADING = 12
let DEXKEY_LOADCELLTARE = 14
let DEXKEY_STEPS = 15 
let DEXKEY_MOTORENABLE = 16 

let dex = {}

let lsq = new LeastSquares()
let oldReadings = [[25, 14854, 29649, 44453, 74061, 103695],
[0, -0.100, -0.200, -0.300, -0.500, -0.700]]
let calibReadings = [[1,2,3], [1,2,3]]
let newReadings = [
    [42, 255872, 209341, 171922, 141727, 108767, 79558, 48830, 30461],
    [0, -16.8854, -13.7983, -11.3386, -9.3394, -7.1638, -5.243, -3.2144, -2.009]]
  //[0, 1723g, 1408g, 1157, 953, 731, 535, 328, 205]] // (gf)
// donate some x, y readings to calibrate the least squares 
// here this was observed integer outputs from the amp, and load applied during those readings
lsq.setObservations(newReadings)
// loadcells need to be calibrated, 
dex.readLoadcell = () => {
  return new Promise((resolve, reject) => {
    if(!wscPort.send){
      reject('port to dex is closed')
      return
    }
    wscPort.send((Uint8Array.from([DEXKEY_LOADCELLREADING])))
    let rejected = false 
    dex.recv = (data) => {
      if(rejected) return // if already timed out... don't get confused 
      if(data[0] != DEXKEY_LOADCELLREADING){
        reject('oddball key after loadcell request')
      } else {
        let counts = TS.read('int32', data, 1, true)
        resolve(lsq.predict(counts))
      }
    }
    setTimeout(() => {
      rejected = true 
      reject('timeout')
    }, TIMEOUT_MS)
  })
}

dex.tareLoadcell = () => {
  return new Promise((resolve, reject) => {
    if(!wscPort.send){
      reject('port to dex is closed')
      return
    }
    wscPort.send(Uint8Array.from([DEXKEY_LOADCELLTARE]))
    let rejected = false 
    dex.recv = (data) => {
      if(rejected) return 
      if(data[0] != DEXKEY_LOADCELLTARE){
        reject('oddball key after loadcell tare request')
      } else {
        resolve()
      }
    }
    setTimeout(() => {
      rejected = true 
      reject('timeout')
    }, TIMEOUT_MS)
  })
}

dex.setMotorEnable = (val) => {
  return new Promise((resolve, reject) => {
    if(!wscPort.send){
      reject('port to dex is closed')
      return
    }
    let ob = 0
    if(val) ob = 1
    wscPort.send(Uint8Array.from([DEXKEY_MOTORENABLE, ob]))
    let rejected = false 
    dex.recv = (data) => {
      if(rejected) return 
      if(data[0] != DEXKEY_MOTORENABLE){
        reject('oddball key after motor enable request')
      } else {
        if(data[1] > 0){
          resolve(true)
        } else {
          resolve(false)
        }
      }
    } // end recv 
    setTimeout(() => {
      rejected = true 
      reject('timeout')
    }, TIMEOUT_MS)
  })
}

// mm -> steps, 
// microsteps are 16
// pinion is 20T, driven is 124
// lead screw is 1204: 4mm / revolution, 
dex.spmm = (200 * 16 / (20/124)) / 4
dex.maxspeed = 0.5 //mm/s it's slow !
console.log('SPMM', dex.spmm)
dex.step = (mm) => {
  // assert maximum move increment here, 
  if(mm > 20) mm = 20 
  if(mm < -20) mm = -20 
  return new Promise((resolve, reject) => {
    if(!wscPort.send){
      reject('port to dex is closed')
      return
    }
    // calculate for spmm, 
    let steps = Math.floor(- dex.spmm * mm)
    //    console.log('STEPS', steps)
    let req = new Uint8Array(5)
    req[0] = DEXKEY_STEPS
    TS.write('int32', steps, req, 1, true)
    wscPort.send(req)
    let rejected = false 
    dex.recv = (data) => {
      if(rejected) return 
      if(data[0] != DEXKEY_STEPS){
        reject('oddball key after step request')
      } else {
        let increment = TS.read('int32', data, 1, true)
        resolve(- increment / dex.spmm)
      }
    } // end recv 
    let moveTime = (Math.abs(mm / dex.maxspeed) + 5) * 1000
    //console.log('TIME', moveTime)
    setTimeout(() => {
      rejected = true 
      reject('timeout')
    }, moveTime)
  })
}

// -------------------------------------------------------- SPAWNING TEST SUBS

let wscPort = {}
wscPort.send = null 
wscPort.onReceive = (data) => {
  // put ll-msg catch flag here, 
  if(data[0] == DEXKEY_DEBUGMSG){
    let msg = TS.read('string', data, 1, true)
    console.warn('DEX DEBUGMSG: ', msg.value)
  } else {
    dex.recv(data)
  }
}
let LOGPHY = false

// to test these systems, the client (us) will kickstart a new process
// on the server, and try to establish connection to it.
console.log("making client-to-server request to start remote process,")
console.log("and connecting to it w/ new websocket")
// ok, let's ask to kick a process on the server,
// in response, we'll get it's IP and Port,
// then we can start a websocket client to connect there,
// automated remote-proc. w/ vPort & wss medium,
// for args, do '/processName.js?args=arg1,arg2'
jQuery.get('/startLocal/dex-usb-bridge.js', (res) => {
  if (res.includes('OSAP-wss-addr:')) {
    let addr = res.substring(res.indexOf(':') + 2)
    if (addr.includes('ws://')) {
      console.log('starting socket to remote at', addr)
      let ws = new WebSocket(addr)
      ws.onopen = (evt) => {
        wscPort.send = (buffer) => {
          if (LOGPHY) console.log('PHY WSC Send', buffer)
          ws.send(buffer)
        }
        ws.onmessage = (msg) => {
          msg.data.arrayBuffer().then((buffer) => {
            let uint = new Uint8Array(buffer)
            if (LOGPHY) console.log('PHY WSC Recv')
            if (LOGPHY) TS.logPacket(uint)
            wscPort.onReceive(uint)
          }).catch((err) => {
            console.error(err)
          })
        }
        UI.start(dex)
      }
      ws.onerror = (err) => {
        console.error('sckt err', err)
        UI.connectionError('socket closed')
      }
      ws.onclose = (evt) => {
        console.error('sckt closed', evt)
        wscPort.send = null
        UI.connectionError('socket closed')
      }
    }
  } else {
    console.error('remote OSAP not established', res)
  }
})