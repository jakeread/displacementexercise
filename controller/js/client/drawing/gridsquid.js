/*
gridsquid.js

osap tool drawing set

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the open systems assembly protocol (OSAP) project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

'use strict'

import dt from './domtools.js'

export default function GRIDSQUID() {
  // ------------------------------------------------------ PLANE / ZOOM / PAN
  let plane = $('<div>').addClass('plane').get(0)
  let wrapper = $('#wrapper').get(0)
  // odd, but w/o this, scaling the plane & the background together introduces some numerical errs,
  // probably because the DOM is scaling a zero-size plane, or somesuch.
  $(plane).css('background', 'url("/client/drawing/bg.png")').css('width', '100px').css('height', '100px')
  let cs = 1 // current scale,
  let dft = { s: cs, x: 600, y: 600, ox: 0, oy: 0 } // default transform

  // zoom on wheel
  wrapper.addEventListener('wheel', (evt) => {
    evt.preventDefault()
    evt.stopPropagation()

    let ox = evt.clientX
    let oy = evt.clientY

    let ds
    if (evt.deltaY > 0) {
      ds = 0.025
    } else {
      ds = -0.025
    }

    let ct = dt.readTransform(plane)
    ct.s *= 1 + ds
    ct.x += (ct.x - ox) * ds
    ct.y += (ct.y - oy) * ds

    // max zoom pls thx
    if (ct.s > 1.5) ct.s = 1.5
    if (ct.s < 0.05) ct.s = 0.05
    cs = ct.s
    dt.writeTransform(plane, ct)
    dt.writeBackgroundTransform(wrapper, ct)
  })

  // pan on drag,
  wrapper.addEventListener('mousedown', (evt) => {
    evt.preventDefault()
    evt.stopPropagation()
    dt.dragTool((drag) => {
      drag.preventDefault()
      drag.stopPropagation()
      let ct = dt.readTransform(plane)
      ct.x += drag.movementX
      ct.y += drag.movementY
      dt.writeTransform(plane, ct)
      dt.writeBackgroundTransform(wrapper, ct)
    })
  })

  // init w/ defaults,
  dt.writeTransform(plane, dft)
  dt.writeBackgroundTransform(wrapper, dft)

  $(wrapper).append(plane)

  // ------------------------------------------------------ RENDER / RERENDER
  // all nodes render into the plane, for now into the wrapper
  // once ready to render, heights etc should be set,
  let renderNode = (node) => {
    let nel = $(`<div>`).addClass('node').get(0) // node class is position:absolute
    nel.style.width = `${parseInt(node.width)}px`
    nel.style.height = `${parseInt(node.height)}px`
    nel.style.left = `${parseInt(node.pos.x)}px`
    nel.style.top = `${parseInt(node.pos.y)}px`
    $(nel).append($(`<div>${node.name}</div>`).addClass('nodename'))
    if (node.el) $(node.el).remove()
    node.el = nel
    $(plane).append(node.el)
  }

  let renderVPort = (vPort, outgoing) => {
    let nel = $('<div>').addClass('vPort').get(0)
    nel.style.width = `${parseInt(vPort.parent.width) - 4}px`
    let ph = perPortHeight - heightPadding
    nel.style.height = `${parseInt(ph)}px`
    nel.style.left = `${parseInt(vPort.parent.pos.x)}px`
    let ptop = vPort.parent.pos.y + heightPadding + vPort.indice * perPortHeight + heightPadding / 2
    nel.style.top = `${parseInt(ptop)}px`
    $(nel).append($(`<div>${vPort.name}</div>`).addClass('vPortname'))
    // draw outgoing svg,
    if(outgoing){
      // anchor position (absolute, within attached-to), delx, dely
      let line = dt.svgLine(perNodeWidth - 2, ph / 2, linkWidth, 0, 2)
      $(nel).append(line) // appended, so that can be rm'd w/ the .remove() call
    }
    if (vPort.el) $(vPort.el).remove()
    vPort.el = nel
    $(plane).append(vPort.el)
  }

  // draw vals,
  let perNodeWidth = 60
  let linkWidth = 30
  let perPortHeight = 120
  let heightPadding = 10

  // for now, this'll look a lot like thar recursor,
  // and we'll just do it once, assuming nice and easy trees ...
  this.draw = (root) => {
    let start = performance.now()
    $('.node').remove()
    $('.vPort').remove()
    // alright binches lets recurse,
    // node-to-draw, vPort-entered-on, depth of recursion
    let recursor = (node, entry, entryTop, depth) => {
      node.width = perNodeWidth // time being, we are all this wide
      node.height = heightPadding * 2 + node.vPorts.length * perPortHeight // 10px top / bottom, 50 per port
      node.pos = {}
      node.pos.x = depth * (perNodeWidth + linkWidth) // our x-pos is related to our depth,
      // and the 'top' - now, if entry has an .el / etc data - if ports have this data as well, less calc. here
      if (entry) {
        node.pos.y = entryTop - entry.indice * perPortHeight - heightPadding
      } else {
        node.pos.y = 0
      }
      // draw ready?
      renderNode(node)
      // traverse,
      for (let vp of node.vPorts) {
        if (vp == entry) {
          renderVPort(vp)
          continue
        } else if (vp.reciprocal) {
          renderVPort(vp, true)
          recursor(vp.reciprocal.parent, vp.reciprocal, node.pos.y + heightPadding + vp.indice * perPortHeight, depth + 1)
        } else {
          renderVPort(vp)
        }
      }
    }
    recursor(root, null, 0, 0)
    //console.warn('draw was', performance.now() - start)
    // root.pos = {
    //   x: 10,
    //   y: 10
    // }
    // root.width = 100
    // root.height = 100
    // renderNode(root)
    // console.warn('next call')
    // root.pos.x = 50
    // renderNode(root)
  }
}
