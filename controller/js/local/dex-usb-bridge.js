/*
dex-usb-bridge.js

dex bridge to firmwarelandia

Jake Read at the Center for Bits and Atoms
(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and
displayed for any purpose, but must acknowledge the open systems assembly protocol (OSAP) project.
Copyright is retained and must be preserved. The work is provided as is;
no warranty is provided, and users accept all liability.
*/

// big s/o to https://github.com/standard-things/esm for allowing this
import WSSPipe from './common/wssPipe.js'

import child_process from 'child_process'

import SerialPort from 'serialport'
import Delimiter from '@serialport/parser-delimiter'
import ByteLength from '@serialport/parser-byte-length'

import COBS from './common/cobs.js'

import { PerformanceObserver, performance } from 'perf_hooks'

import { TS } from '../core/ts.js'

let LOGPHY = false

// -------------------------------------------------------- WSS VPort

let wssPort = {}
wssPort.send = null 

WSSPipe.start().then((ws) => {
	// have ws.send()
	wssPort.send = (data) => {
		ws.send(data)
	}
	ws.onmessage = (msg) => {
		if (LOGPHY) console.log('WSS Recv')
		if (LOGPHY) TS.logPacket(msg.data)
		// would forward blind to usb, if possible 
		// ws.send(msg.data) // echo 
		if(serPort.send){
			if(LOGPHY) console.log('forwarding wss -> usb...')
			serPort.send(msg.data)
		}	
	}
	ws.onerror = (err) => {
		console.log('WSS ERROR', err)
		console.log('exiting')
		process.exit()
	}
	ws.onclose = (evt) => {
		console.log('WSS CLOSES, exiting')
		process.exit()
	}
})

// -------------------------------------------------------- USB Serial VPort

let serPort = {}
serPort.send = null 

let findSerialPort = (pid) => {
	console.log(`SERPORT hunt for productId: ${pid}`)
	return new Promise((resolve, reject) => {
		SerialPort.list().then((ports) => {
			let found = false
			for (let port of ports) {
				if (port.productId === pid) {
					found = true
					resolve(port.path)
					break
				}
			}
			if (!found) reject(`serialport w/ productId: ${pid} not found`)
		}).catch((err) => {
			reject(err)
		})
	})
}

// options: passthrough for node-serialport API
let startSerialPort = (pid, options) => {
	findSerialPort(pid).then((com) => {
		if (true) console.log(`SERPORT contact at ${com}, opening`)
		let port = new SerialPort(com, options)
		port.on('open', () => {
			// to get, use delimiter
			let parser = port.pipe(new Delimiter({ delimiter: [0] }))
			//let parser = port.pipe(new ByteLength({ length: 1 }))
			parser.on('data', (buf) => {
				let decoded = COBS.decode(buf)
				if (LOGPHY) {
					console.log('SERPORT Decoded', decoded)
				}
				if(wssPort.send){
					if(LOGPHY) console.log('forwarding usb -> wss...')
					wssPort.send(decoded)
				}
			})
			// to ship, 
			serPort.send = (buffer) => {
				if (LOGPHY) {
					console.log('SERPORT Tx Encoded')
					TS.logPacket(COBS.encode(buffer))
				}
				port.write(COBS.encode(buffer))
			}
		})
		port.on('error', (err) => {
			console.log('SERPORT ERR', err)
		})
	}).catch((err) => {
		console.log(`SERPORT cannot find device at ${pid}`, err)
	})
}

// startup serialport search on launch 
startSerialPort('8031')

/*
SerialPipe.start('8031').then((port) => {
  console.log('open ok, setup...')
  let ship = (arr) => {
    //console.log('encoding', arr)
    let encoded = COBS.encode(arr)
    // should be a zero-term'd buffer ?
    console.log('spcobs writing', encoded)
    port.write(encoded, 'utf8')
  }
  serialPortVPort.take(ship)
  port.on('error', (err) => {
    console.log('serialport err', err)
    process.exit()
  })
//  let parser = port.pipe(new ByteLength({length: 1}))
  let parser = port.pipe(new Delimiter({ delimiter: [0] }))
  parser.on('data', (buf) => {
    let decoded = COBS.decode(buf)
    let dcarr = []
    for(let i = 0; i < decoded.length; i ++){
      dcarr.push(decoded[i])
    }
    // ok, dcarr (decoded-array) is it,
    console.log('spcobs recv', dcarr)
    serialPortVPort.recv(dcarr)
  })
  // OK
  // now we need to write just a barebones COBS catch-and-rx
  // and then send some test packets thru the link, yeah?
  // .take(port) should actually be .take(send)
  // where let send = (arr) =>{cobs(arr)->port}
  // to test,
  // serialPortVPort.send([], [0, 2, 3]).then((ack) => {
  //   if (ack.msg[0] == PKEYS.ERR) {
  //     let str = String.fromCharCode.apply(null, ack.msg.splice(1))
  //     console.error('remote repl with error', str)
  //   } else {
  //     console.log('ok, port returns...', ack.msg, ack.route)
  //   }
  // }).catch((err) => {
  //   console.log(err)
  // })
}).catch((err) => {
  //
})
*/
