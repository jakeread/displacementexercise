# The Displacement Exercise (DEX)

The DEX is an open source piece of materials testing equipment. The machine can be manufactured by anyone with access to a laser cutter with at least a 24x12" bed, or a milling machine of similar or larger size, and nearly any FDM 3D Printer. A bill of materials of purchased parts required to complete the machine is below, totaling some ~ $500 USD.

The machine uses a NEMA 23 stepper motor and a small ball screw to exert force on samples, and any off the shelf load-cell to measure stress. At the lab, we use [squidworks](https://gitlab.cba.mit.edu/squidworks/squidworks) controllers to operate the machine, but a standalone controller written for Arduino and with a JS frontend is [here](controller). 

![dex](images/2020-02-11_dex-render.png)

![dex-alive](images/2020-07-12_dex-on-desk-01.jpg)

![control](images/2020-07-11_dex-tools.png)

## Operating Principle

A [stress - strain plot](https://en.wikipedia.org/wiki/Stress%E2%80%93strain_curve) is a very useful piece of information when characterizing materials.

![ss curve](images/stress-strain.jpg)

To generate these curves, the DEX slowly pulls samples (normally some 'dogbone' shape [below](#testing-notes)) apart, while measuring the amount that it stretches (~ the strain), and the amount of force it exerts as it is stretched (~ the stress). These types of machine are common in industry, often referred to by their leading brand name 'Instron', or as 'Universal Testing Machines' (UTM).

## Comparison to the Instron 4411

I compared results from the most recent DEX build to the Instron 4411 we have in the lab. Some errors still exist: it could be the case that the load cell needs better calibration: my largest calibration weights are near 2kg, but these loads approach 8kgf. I'm not sure how much nonlinearity exists in COTS (and cheap) loadcells like this, but I have *heard* they are *fairly* linear, so certainly we can do better than heresay. There is certainly some flex in the machine itself, but much less than earlier versions. To improve this, the DEX can be fabricated in Phenolic or some other composite, rather than Acrylic as used here. Finally, fixturing could be greatly improved, I believe some creep exists in the jaws as well. I have ordered some parts to test better (off the shelf) steel clamps. 

![dt1](data/2020-07-12_compare-dex-4411/2020-07-12_compare-dex-4411-01.png)
![dt2](data/2020-07-12_compare-dex-4411/2020-07-12_compare-dex-4411-02.png)

## Hardware

### CAD

CAD for the machine is available in this Repo, [under `cad/`](cad/) - the `.f3z` file is a Fusion 360 parametric model of the machine, and the `.3dm` model is a complete Rhino3D Model, with purchase parts included. To source parts, consult the BOM below.

![dex](images/2020-02-11_dex-parts.png)

### BOM

Part numbers are from [McMaster Carr](http://mcmaster.com) unless otherwise linked.

| Part | Spec | Count | Notes | PN / Link | Cost / Per |
| --- | --- | ---: | --- | ---: | ---: |
| Acrylic Sheet | 24x12", 0.25" Thick | 2 | - | 8505K755 | $19.93 / 1 |
| PLA 'Tough' |  | ~ 300g | Many 3DP Mechanical Bits | [Matter Hackers M6E9T65K](https://www.matterhackers.com/store/l/light-blue-pro-series-tough-pla-filament-175mm-1kg/sk/M6E9T65K) | $52.00 / 1kg |
| 625ZZ Bearings | 5x16x5  | 13 | - | [VXB 625ZZ](https://www.vxb.com/20-625ZZ-Shielded-5mm-Bore-Diameter-Miniature-p/625zz20.htm) | $24.95 / 20 |
| Bearing Shims | 5x10x0.5 | 26 | - | 98055A098 | $8.62 / 50 |
| Carriage Shoulders | M4x5x6 | 13 | - | 92981A146 | $2.16 / 1 |
| NEMA 23 Stepper Motor | 56mm Can Length | 1 | Spec Shaft with Pinion | [Stepper Online](https://www.omc-stepperonline.com/nema-23-stepper-motor/nema-23-bipolare-1-8deg-1-26nm-178-4oz-in-2-8a-2-5v-57x57x56mm-4-fili.html) | $14.83 / 1 |
| GT2 Pinion | 20T | 1 | Spec Shaft with NEMA 23 | [Amazon](https://www.amazon.com/Saiper-GT2-Teeth-6-35mm-Synchronous/dp/B07MGMBX3N/) (or) [RobotDigg](https://www.robotdigg.com/product/226/20-Tooth-2GT-Pulley-10pcs-per-lot) | $9.96 / 5 |
| GT2 Closed Loop Belt | 200T (400mm long), 6mm Wide | 1 | - | [Amazon](https://www.amazon.com/400-2GT-6-Timing-Belt-Closed-Loop/dp/B014U7OSVA/) (or) [RobotDigg](https://www.robotdigg.com/product/286) | $15.88 / 10 |
| 1204 x 400mm SFU Ball Screw Kit | | 1 | | [Amazon](https://www.amazon.com/SFU1204-Ballscrew-RM1204-Housing-Machine/dp/B076PCVC8F/)
| M3 Inserts | Tapered, 3.8mm Tall | 200 | - | 94180A331 | $12.92 / 100 |
| M4 Inserts | Tapered, 4.7mm Tall | 16 | - | 94180A351 | $14.96 / 100 |
| SHCS | M3x16 | 100 | Pinning T- to the wall, and pulley endmatter | 91292A115 | $5.87 / 100 |
| M3 Washers | 18-8 Standard Flat | 300 | - | 93475A210 | $1.62 / 100 |
| SHCS | M4x16 | 6 | Mounting Ball Nut | 91292A118 | $8.13 / 100 |
| SHCS | M5x20 | 4 | Motor Mounting | 91292A121 | $9.03 / 100 |
| Locknut | M5 | 4 | Motor Mounting | 93625A200 | $6.46 / 100 |
| SHCS | M8x30 | 2 | Loadcell Fixturing | 91292A149 | $10.13 / 25 |
| Feet | M4 Stud, 15x15mm | 3 | - | 93115K881 | $1.78 / 1 |
| Shaft Collar | 8mm Diameter, Flanged | - | - | 9723T12 | $60.14 |
| Loadcell(s) | 10, 30, or 50kg | 1 | Choose Range for Sensitivity | [Amazon 50kg](https://www.amazon.com/Pressure-Force-S-type-Sensor-Cable/dp/B01F6IOW3G/) [Amazon 30kg](https://www.amazon.com/Pressure-Force-S-type-Sensor-Cable/dp/B01F6IOWDG/) [Amazon 10kg](https://www.amazon.com/Pressure-Force-S-type-Sensor-Cable/dp/B01F6IOW4K/) | $39.00 / 1|
| Loadcell Amplifier | HX711 | 1 | - | [Sparkfun 13879](https://www.sparkfun.com/products/13879) | $9.95 / 1 |
| Power Supply | 350W 24V LRS-350-24 | 1 | - | [Amazon](https://www.amazon.com/MEAN-WELL-LRS-350-24-350-4W-Switchable/dp/B013ETVO12/) | $32.25 / 1 |

### Choice of Sheet Material

Here, I list Acrylic Sheet as the main chassis material. With acrylic, the machine can be manufactured easily on a laser cutter. However, it should also be possible to build the DEX with different sheet material. Lately, I have been using Phenolic as a chassis material: it can be milled on a ShopBot or similar router, and performs much better than Acrylic, with around 11GPA flexural strength (vs. Acrylic's ~ 3GPA). Acrylic is best bonded with Acrylic Cement, a solvent that welds the plastic to itself. Phenolic is best bonded with a two-part epoxy: I use a 20 minute working life epoxy, and nearly any type will do.

## Control and Interface: Standalone 

I recently built a [standalone controller](controller) for this thing, it's written in Arduino and vanilla JS for Node (to serve the app and connect to the machine) and the Browser. 

![dex-alive](images/2020-07-12_dex-on-desk-02.jpg)

## Control and Interface: Squidworks

DEX is designed to run a [squidworks](https://gitlab.cba.mit.edu/squidworks/squidworks) controller. The `dex` branches of [cuttlefish](https://gitlab.cba.mit.edu/squidworks/cuttlefish) and [ponyo](https://gitlab.cba.mit.edu/squidworks/ponyo) contain code that is known to work with the machine. For more info on the controllers, please consult those repositories. In all, controlling the machine can be achieved in any number of ways, on needs only to control one stepper motor and read one load cell.

![c1](images/2019-10-17_dex-controller.png)
![c2](images/2019-10-17_dex-controller-zoom.png)

#### Vision for Displacement Sensing

We're currently working to build a computer vision based displacement sensing method for the DEX. Since our machine (or, many machines manufactured by novices / in the public domain) are liable to flex (indeed, nothing is infinitely stiff!), the thought is to measure local displacements of the sample, at the sample, rather than measuring open-loop through the machine's structure. I started this with a small sample code, noted in [the log](LOG.md), and have gone through integrating this into the machine's controller.

![dex-vision](images/2019-11_dex-cam.jpg)

The controller involves a calibration stage, where I operate the machine in open-loop, stepping in increments with no load exerted on the machine. I track the marker's displacements, building a map between the known ballscrew displacements and the tracked pixel positions.

![dex-calib](images/2019-11_dex-vision-calibration.png)

Once I have this calibration, I save the function fit, and use it to read positions back while the machine is loaded.

![dex-use](images/2019-11_dex-vision-controller.png)

This nearly works, but needs some improvement. To go about doing this at a higher fidelity, I am working on integrating Python scripts into squidworks controllers, which will let me tie OpenCV codes into dataflow controllers. Another escalation will involve using DIC (digital image correlation) codes to observe the entire structure's deflections during testing, which should render even higher quality testing results.

## Testing Notes

The D683 ASTM Dogbones:

![dogbones](images/astm_d-638_bonesizes.jpg)
